<?php

namespace GreenstoneMedia\Identifiable\Models;

use App\Models\User;
use Illuminate\Database\Eloquent\Model;

class Identity extends Model
{
    public $table = "identities";

    public $primaryKey = "id";

    public $timestamps = false;

    public $fillable = [
        'user_id',
        'name',
        'identity'
    ];

    public static $rules = [
        'user_id' => 'required',
        'name'=>'required',
        'identity'=>'required'
    ];

    /**
     * User model for the identity
     *
     * @return Relationship
     */
    public function user(){
        return $this->belongsTo(config('identifiable.user_model'));
    }
}
