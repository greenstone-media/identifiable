<?php

namespace GreenstoneMedia\Identifiable\Interfaces;

interface Identifiable {
    /**
     * Determines if the user has the identity that uses the
     * supplied name
     *
     * @param string $name
     * 
     * @return boolean
     */
    public function hasIdentity($name);

    /**
     * Retrieves the supplied named identity for the user, or null
     * if not found
     *
     * @param string $name
     * 
     * @return GreenstoneMedia\Identifiable\Models\Identity|null
     */
    public function getIdentity($name);

    /**
     * Sets an identity for the supplied user
     *
     * @param integer $user_id
     * @param string $name
     * @param string $identity_token
     * 
     * @return GreenstoneMedia\Identifiable\Models\Identity
     */
    public function setIdentity($name, $identity_token);

    /**
     * Removes the identity from the supplied user
     *
     * @param integer $user_id
     * @param string $name
     * 
     * @return boolean
     */
    public function removeIdentity($name);
}