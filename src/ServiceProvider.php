<?php 

namespace GreenstoneMedia\Identifiable;

use Illuminate\Support\ServiceProvider as LaravelServiceProvider;

class ServiceProvider extends LaravelServiceProvider {

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot() {

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        $this->handleConfigs();
        $this->handleMigrations();
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides() {

        return [];
    }

    private function handleConfigs() {

        $configPath = __DIR__ . '/../config/identifiable.php';

        $this->publishes([$configPath => config_path('identifiable.php')]);

        $this->mergeConfigFrom($configPath, 'identifiable');
    }

    private function handleMigrations() {
        $this->publishes([
            __DIR__ . '/migrations' => $this->app->databasePath() . '/migrations'
        ], 'migrations');

        $this->loadMigrationsFrom(__DIR__.'/migrations');
    }
}
