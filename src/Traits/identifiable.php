<?php

namespace GreenstoneMedia\Identifiable\Traits;

use GreenstoneMedia\Identifiable\Models\Identity;

trait Identifiable{
    public function hasIdentity($name)
    {
        return Identity::where([
            'user_id' => $this->id,
            'name' => $name
        ])->count() > 0;
    }

    public function getIdentity($name)
    {
        return Identity::where([
            'user_id' => $this->id,
            'name' => $name
        ])->first();
    }

    public function setIdentity($name, $identity_token)
    {
        if($this->hasIdentity($name)){
            $identity = $this->getIdentity($name);

            $identity->update([
                'identity'=>$identity_token
            ]);

            return $this->getIdentity($name);
        } else {
            return Identity::create([
                'user_id'=>$this->id,
                'name'=>$name,
                'identity'=>$identity_token
            ]);
        }
    }
    public function removeIdentity($name)
    {
        return Identity::where([
            'user_id'=>$this->id,
            'name'=>$name
        ])->delete();
    }
}