<?php

return [
    'user_model' => env('IDENTIFIABLE_USER_MODEL', \App\Models\User::class)
];
